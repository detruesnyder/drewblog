import Vue from 'vue'
import Vuex from 'vuex'
import allBlogs from './../assets/blogs.json'

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    state: {
      blogs: [],
      featuredBlog: {},
      tags: {},
      filters: [],
    },

    actions: {
      LOAD_BLOGS({ commit }) {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            var blogs = []
            var tags = {}
            var featuredBlog = {}
            var allTags = []
            for (var i = 0; i < allBlogs.length; i++) {
              for (var x = 0; x < allBlogs[i].tags.length; x++) {
                allTags.push(allBlogs[i].tags[x])
              }
              if (allBlogs[i].featured) {
                featuredBlog = allBlogs[i]
                continue
              } else {
                blogs.push(allBlogs[i])
              }
            }
            for (var i = 0; i < allTags.length; i++) {
              if (tags.hasOwnProperty(allTags[i])) {
                tags[allTags[i]]++
              } else {
                tags[allTags[i]] = 1
              }
            }
            commit('SET_FEATUREDBLOG', featuredBlog)
            commit('SET_BLOGS', blogs)
            commit('SET_TAGS', tags)
            resolve()
          }, 1000)
        })
      },
      LOAD_FILTERS( store, commit ) {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            var filters = []
            var tags = store.getters.GET_TAGS
            for (let tag in tags) {
              var obj = {label: tag, count: tags[tag], isActive: true, color: 'blue', avatarColor: 'blue darken-4'}
              filters.push(obj)
            }
            store.commit('SET_FILTERS', filters)
            resolve()
          }, 1000)
        })
      }
    },

    mutations: {
      SET_BLOGS(state, blogs) {
        state.blogs = blogs
      },
      SET_FEATUREDBLOG(state, blog) {
        state.featuredBlog = blog
      },
      SET_TAGS(state, tags) {
        state.tags = tags
      },
      SET_FILTERS(state, filters) {
        state.filters = filters
      }
    },

    getters: {
      GET_BLOGS: state => {
        return state.blogs
      },
      GET_FEATUREDBLOG: state => {
        return state.featuredBlog
      },
      GET_TAGS: state => {
        return state.tags
      },
      GET_FILTERS: state => {
        return state.filters
      }
    }
  })
}
