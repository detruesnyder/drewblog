import BlogList from './BlogList'
import BlogCard from './BlogCard'
import BlogFeaturedPost from './BlogFeaturedPost'

export default {
  BlogFeaturedPost,
  BlogList,
  BlogCard
}
